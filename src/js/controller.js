import * as model from './model';
import favoritesView from './views/favoritesView';
import recipeView from './views/recipeView';
import resultsView from './views/resultsView';
import searchView from './views/searchView';

const controlRecipes = async function () {
  // 1) Loading recipe
  try {
    const recipeId = window.location.hash.slice(1);

    if (!recipeId) {
      return;
    }

    recipeView.renderSpinner();
    await model.loadRecipe(recipeId);

    // 2) Rendering recipe
    recipeView.render(model.state.recipe);
    recipeView.addHandlerFavorite(controlFavorite);
  } catch (err) {
    recipeView.renderError();
  }
};

const controlSearchResults = async function () {
  model.state.recipes_view = model.RECIPES_VIEW_SEARCH;
  try {
    const query = searchView.getQuery();

    if (!query) {
      return;
    }
    resultsView.renderSpinner();
    await model.loadSearchResults(query);
    resultsView.render(model.state.search.results);
  } catch (err) {
    console.log(err);
  }
};

const controlShowFavorites = async function () {
  model.state.recipes_view = model.RECIPES_VIEW_FAVORITES;
  resultsView.render(model.state.favorites);
};

const controlFavorite = async function (recipe) {
  if (!model.state.favorites.find(favoriteRecipe => favoriteRecipe.id === recipe.id)) {
    model.addToFavorites(recipe);
  } else {
    model.removeFromFavorites(recipe);
  }

  if (model.state.recipes_view === model.RECIPES_VIEW_FAVORITES) {
    resultsView.render(model.state.favorites);
  }
};


const init = function () {
  recipeView.addHandlerRender(controlRecipes);
  searchView.addHandlerSearch(controlSearchResults);
  favoritesView.addHandlerFavorites(controlShowFavorites);
};

init();
