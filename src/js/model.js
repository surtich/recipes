import { API_URL } from './config';
import { getJSON } from './helpers';

export const RECIPES_VIEW_SEARCH = "RECIPES_VIEW_SEARCH";
export const RECIPES_VIEW_FAVORITES = "RECIPES_VIEW_FAVORITES";

export const state = {
  recipes_view: RECIPES_VIEW_SEARCH,
  recipe: {},
  search: {
    query: '',
    results: [],
  },
  favorites: []
};

export const loadRecipe = async function (recipeId) {
  try {
    const data = await getJSON(`${API_URL}/${recipeId}`);

    state.recipe = {
      id: data.id,
      title: data.title,
      publisher: data.publisher,
      sourceUrl: data.source_url,
      image: data.image_url,
      servings: data.servings,
      cookingTime: data.cooking_time,
      ingredients: data.ingredients,
    };

    console.log(state.recipe);
  } catch (err) {
    console.log(`${err} 💥💥💥💥`);
    throw err;
  }
};

export const loadSearchResults = async function (query) {
  try {
    state.search.query = query;
    const data = await getJSON(`${API_URL}?title_like=${query}`);

    state.search.results = data.map(rec => {
      return {
        id: rec.id,
        title: rec.title,
        publisher: rec.publisher,
        image: rec.image_url,
      };
    });
  } catch (err) {
    console.log(`${err} 💥💥💥💥`);
    throw err;
  }
};

export function addToFavorites(recipe) {
  if (!state.favorites.find(favoriteRecipe => favoriteRecipe.id === recipe.id)) {
    state.favorites.push(recipe);
  }
}


export function removeFromFavorites(recipe) {
  state.favorites = state.favorites.filter(favoriteRecipe => favoriteRecipe.id !== recipe.id);
}
