class FavoritesView {
  #element = document.querySelector('#btn-favorites');


  addHandlerFavorites(handler) {
    this.#element.addEventListener('click', function (e) {
      e.preventDefault();
      handler();
    });
  }
}

export default new FavoritesView();
